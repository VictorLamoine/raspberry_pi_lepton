#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QImage>

class QLabel;
class LeptonThread;
class QGridLayout;

class MainWindow : public QMainWindow
{
  Q_OBJECT

  enum { ImageWidth = 320, ImageHeight = 240 };

  int snapshot_count_ = 0;

  QLabel *image_label_;
  LeptonThread *thread_;
  QGridLayout *layout_;

  unsigned short raw_min_, raw_max_;
  QVector<unsigned short> raw_data_;
  QImage rgb_image_;

public:
  explicit MainWindow(QWidget *parent = 0);

public slots:
  void saveSnapshot();
  void updateImage(unsigned short *, int, int);
};

#endif

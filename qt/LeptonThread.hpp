#ifndef LEPTON_THREAD_HPP
#define LEPTON_THREAD_HPP

#include <fcntl.h>
#include <getopt.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <QDebug>
#include <QThread>
#include <QVector>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

class LeptonThread : public QThread
{
  Q_OBJECT
public:
  enum
  {
    FrameWidth = 80,
    FrameHeight = 60,
    RowPacketWords = FrameWidth + 2,
    RowPacketBytes = 2 * RowPacketWords,
    FrameWords = FrameWidth * FrameHeight
  };

  LeptonThread();
  ~LeptonThread();

  void run();

signals:
  void updateImage(unsigned short *, int, int);

private:
  QVector<unsigned char> result_;
  QVector<unsigned short> raw_data_;

  const char *device_ = "/dev/spidev0.1";
  const unsigned char mode_ = 0, bits_ = 8;
  const unsigned int speed_ = 16000000;
  const unsigned short delay_ = 0;
  QVector<unsigned char> tx_;
  int fd_;
  struct spi_ioc_transfer tr_;

  bool initLepton();
  int getPacket(int, unsigned char *packet_data);

};

#endif

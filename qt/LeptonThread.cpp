#include "LeptonThread.hpp"
#include <QString>
#include <QTextStream>

LeptonThread::LeptonThread():
  QThread(),
  result_(RowPacketBytes * FrameHeight),
  raw_data_(FrameWords)
{
  tx_ = QVector<unsigned char>(LeptonThread::RowPacketBytes, 0);
}

LeptonThread::~LeptonThread()
{
}

bool LeptonThread::initLepton()
{
  fd_ = open(device_, O_RDWR);
  if (fd_ < 0)
  {
    qDebug() << "Can't open device";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_WR_MODE, &mode_))
  {
    qDebug() << "Can't set SPI mode";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_RD_MODE, &mode_))
  {
    qDebug() << "Can't get SPI mode";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_WR_BITS_PER_WORD, &bits_))
  {
    qDebug() << "Can't set bits per word";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_RD_BITS_PER_WORD, &bits_))
  {
    qDebug() << "Can't get bits per word";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_WR_MAX_SPEED_HZ, &speed_))
  {
    qDebug() << "Can't set max speed";
  }
  else if (-1 == ioctl(fd_, SPI_IOC_RD_MAX_SPEED_HZ, &speed_))
  {
    qDebug() << "Can't get max speed";
  }
  else
  {
    return true;
  }
  return false;
}

int LeptonThread::getPacket(int, unsigned char *packetData)
{
  tr_.rx_buf = (unsigned long) packetData;
  return ioctl(fd_, SPI_IOC_MESSAGE(1), &tr_);
}

void LeptonThread::run()
{
  if (!initLepton())
    return;

  usleep(250000);

  tr_.tx_buf = (unsigned long) &tx_[0];
  tr_.len = RowPacketBytes;
  tr_.delay_usecs = delay_;
  tr_.speed_hz = speed_;
  tr_.bits_per_word = bits_;

  int resets(0); // Number of times we've reset the 0...59 loop for packets
  int errors(0); // Number of error-packets received
  while (true)
  {
    int i_row;
    for (i_row = 0; i_row < FrameHeight;)
    {
      unsigned char *packet = &result_[i_row * RowPacketBytes];

      if (getPacket(i_row, packet) < 1)
      {
        qDebug() << "Error transferring SPI packet";
        return;
      }

      int packet_number;
      if ((packet[0] & 0xf) == 0xf)
      {
        packet_number = -1;
      }
      else
      {
        packet_number = packet[1];
      }

      if (packet_number == -1)
      {
        usleep(1000);
        if (++errors > 300)
        {
          break;
        }
        continue;
      }

      if (packet_number != i_row)
      {
        usleep(1000);
        break;
      }

      ++i_row;
    }

    if (i_row < FrameHeight)
    {
      if (++resets >= 750)
      {
        qDebug() << "Packet reset counter hit 750";
        resets = 0;
        usleep(750000);
      }
      continue;
    }

    resets = 0;
    errors = 0;

    uint16_t min_value = 65535;
    uint16_t max_value = 0;
    unsigned char *in = &result_[0];
    unsigned short *out = &raw_data_[0];
    for (int i_row = 0; i_row < FrameHeight; ++i_row)
    {
      in += 4;
      for (int i_col = 0; i_col < FrameWidth; ++i_col)
      {
        unsigned short value = in[0];
        value <<= 8;
        value |= in[1];
        in += 2;
        if (value > max_value)
        {
          max_value = value;
        }
        if (value < min_value)
        {
          min_value = value;
        }
        *(out++) = value;
      }
    }

    emit updateImage(&raw_data_[0], min_value, max_value);
  }
}

Do **NOT** use a `aarch64` (64 bits) image of Raspberry Pi OS/ Ubuntu

Run the programs with `sudo` unless you allow using SPI without root:

```bash
echo 'SUBSYSTEM=="spidev", GROUP="spiuser", MODE="0660"' | sudo tee /etc/udev/rules.d/50-spi.rules
sudo groupadd spiuser
sudo adduser $USER spiuser
```

Reboot after these commands
